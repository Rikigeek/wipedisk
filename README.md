Espace de travail ~/temp/TC
Basé sur Tiny core linux (http://www.tinycorelinux.net/)

# Démarrage 
```
qemu-img create -f raw OS.disk 1G
# Boot MBR, non UEFI, sur disk SATA
qemu-system-x86_64 -cdrom wipedisk-v1.2.iso -m 1024 -s OS.disk
# Boot en SCSI
qemu-system-x86_64 \
     -m 1024 \
     -device virtio-scsi-pci,id=scsi \
     -device scsi-hd,drive=hd \
     -drive if=none,id=hd,file=OS.disk,format=raw \
     -cdrom tmp.iso
     

```
# Construction

Liste des extensions
Utilisation de kmaps.tcz, pour démarrer en mode fr
http://repo.tinycorelinux.net/13.x/x86/tcz/

Config de grub, manuel en ligne
https://www.gnu.org/software/grub/manual/grub/grub.html#timeout

boot/grub/grub.cfg
```
default=0
timeout=0

menuentry 'Core Linux' {
    linux   /tinycorelinux/vmlinuz kmaps=azerty/fr-latin9
    initrd  /tinycorelinux/core.gz
}
```

boot/grub/x86_64-efi/grub.cfg
```
insmod part_gpt
insmod part_msdos
source /boot/grub/grub.cfg
```
A partir de là, tout fonctionne bien

Dans l'arborescence, il faut garder les répertoires : 
- boot
- EFI
- isolinux
Et j'ai ajouté tinycorelinux pour contenir la distribution

## construction de mon iso
```
seb@debian:~/temp/TC$ ./buildiso.sh && ./uploadiso.sh 
xorriso 1.5.2 : RockRidge filesystem manipulator, libburnia project.

Drive current: -outdev 'stdio:tmp.iso'
Media current: stdio file, overwriteable
Media status : is blank
Media summary: 0 sessions, 0 data blocks, 0 data, 20.4g free
Added to ISO image: directory '/'='/home/seb/temp/TC/isov1'
xorriso : UPDATE :      22 files added in 1 seconds
xorriso : UPDATE :      22 files added in 1 seconds
xorriso : NOTE : Copying to System Area: 432 bytes from file '/usr/lib/ISOLINUX/isohdpfx.bin'
libisofs: NOTE : Aligned image size to cylinder size by 479 blocks
xorriso : UPDATE : Thank you for being patient. Working since 0 seconds.
ISO image produced: 13824 sectors
Written to medium : 13824 sectors at LBA 0
Writing to 'stdio:tmp.iso' completed successfully.

tmp.iso                                                               100%   27MB  27.5MB/s   00:00   
```

### Sources des scripts
```
seb@debian:~/temp/TC$ cat buildiso.sh 
#!/bin/sh

# Need to install package isolinux (debian)
MBR=/usr/lib/ISOLINUX/isohdpfx.bin
SRC=isov1/
# all the path are based inside the iso, except for the isohybrid-mbr option
# need to install xorriso package (debian)
xorriso -as mkisofs \
  -isohybrid-mbr ${MBR} \
  -c isolinux/boot.cat \
  -b isolinux/isolinux.bin \
  -no-emul-boot \
  -boot-load-size 4 \
  -boot-info-table \
  -eltorito-alt-boot \
  -e boot/grub/efi.img \
  -no-emul-boot \
  -isohybrid-gpt-basdat \
  -o tmp.iso \
  ${SRC}

seb@debian:~/temp/TC$ cat uploadiso.sh 
#!/bin/sh
scp tmp.iso root@ouranos.local:/var/lib/vz/template/iso
```

### Détails de la commande de création de l'ISO (EFI)
-as mkisofs : personality options - utilise les options de mkisofs
-c isolinux/boot.cat : the "El Torito" boot catalog
-b isolinux/isolinux.bin : the "El Torito" boot image
-no-emul-boot : the "El Torito" bootable CD is a "no emulation" image. no disk emulation is performed while system is loading
-boot-load-size 4 : in no emulation mode, how many virtual sectors (512 bytes) to load. if not a multiple of 4, may be a problem for some BIOS
-boot-info-table : 56 bytes table will be patched in at offset 8 in the boot file. note: boot file is modified in the source filesystem
-eltorito-alt-boot : start with a new set of "El Torito" boot parameters. To have more than one "El Torito" boot on a CD. Max of 63 El Torito boot entries on a single CD
-e boot/grub/efi.img : ? alternative option -e from Fedora genisoimage sets bin_path and platform_id for EFI, but performs no "next".
-no-emul-boot : same as before
-isohybrid-gpt-basdat : 
-o tmp.iso
SRC

La première partie génère la partie "MBR", la seconde (après le "eltorito-alt-boot") génère la partie "EFI".
La partie MBR boot sur ISOLINUX
La seconde sur Grub

pour le boot sur MBR il faut quelques ajustements, en particulier remplir le fichier isolinux.cfg
Il faut aussi ajouter quelques modules, sinon
fail to load ldlinux.c32
cp /usr/lib/syslinux/modules/bios/ldlinux.c32 isov1/isolinux/
cp /usr/lib/syslinux/modules/bios/vesamenu.c32 isov1/isolinux/
cp /usr/lib/syslinux/modules/bios/libcom32.c32 isov1/isolinux/
cp /usr/lib/syslinux/modules/bios/libutil.c32 isov1/isolinux/

Dans isolinux.cfg
```
# D-I config version 2.0
# search path for the c32 support libraries (libcom32, libutil etc.)
path 
include menu.cfg
# default vesamenu.c32
default resetdrive
prompt 0
timeout 0

LABEL resetdrive
  menu label ^Boot and clear hard drive
  kernel /tinycorelinux/vmlinuz
  append kmap=azerty/fr-latin9 initrd=/tinycorelinux/core.gz
```
## Modules à ajouter à TCL
util-linux.tcz (pour lsblk)
Depends on:
readline.tcz
	ncursesw.tcz
udev-lib.tcz
	glib2.tcz
		libffi.tcz
pv.tcz (pour progress sur dd)

```
wget http://www.tinycorelinux.net/13.x/x86/tcz/util-linux.tcz
wget http://www.tinycorelinux.net/13.x/x86/tcz/readline.tcz
wget http://www.tinycorelinux.net/13.x/x86/tcz/ncursesw.tcz
wget http://www.tinycorelinux.net/13.x/x86/tcz/udev-lib.tcz
wget http://www.tinycorelinux.net/13.x/x86/tcz/glib2.tcz
wget http://www.tinycorelinux.net/13.x/x86/tcz/libffi.tcz
wget http://www.tinycorelinux.net/13.x/x86/tcz/pv.tcz
```

## Personnalisation de l'ISO
chaine de scripts: ./buildcore.sh && ./buildiso.sh && ./uploadiso.sh
L'upload l'envoie sur mon proxmox pour pouvoir le tester sur mes vm

```
seb@debian:~/temp/TC/isov1/boot$ cd ../tinycorelinux/
seb@debian:~/temp/TC/isov1/tinycorelinux$ ls
core.gz  vmlinuz
seb@debian:~/temp/TC/isov1/tinycorelinux$ mkdir tmpcore
seb@debian:~/temp/TC/isov1/tinycorelinux$ sudo mount core.gz tmpcore/ -o loop,ro
[sudo] Mot de passe de seb : 
mount: /home/seb/temp/TC/isov1/tinycorelinux/tmpcore: mauvais type de système de fichiers, option erronée, superbloc erroné sur /dev/loop10, page de code ou programme auxiliaire manquant, ou autre erreur.
seb@debian:~/temp/TC/isov1/tinycorelinux$ cd tmpcore/
seb@debian:~/temp/TC/isov1/tinycorelinux/tmpcore$ zcat ../core.gz | sudo cpio -i -H newc -d
33144 blocs
seb@debian:~/temp/TC/isov1/tinycorelinux/tmpcore$ cd ..
seb@debian:~/temp/TC/isov1/tinycorelinux$ mkdir tmpkmaps
seb@debian:~/temp/TC/isov1/tinycorelinux$ sudo mount ../tce/kmaps.tcz tmpkmaps/ -o loop
seb@debian:~/temp/TC/isov1/tinycorelinux$ ls tmpkmaps/
usr
seb@debian:~/temp/TC/isov1/tinycorelinux$ ls tmpkmaps/usr/share/kmap/
azerty/             dvorak/             olpc/               qwertz/             th-win-latin1.kmap
colemak/            fgGIod/             qwerty/             th-latin1.kmap      
```
Puis on peut ajouter dans etc/skel un fichier de script à lancer au démarrage (depuis .profile) pour démarrer le formatage
pour vérifier les disques, regarder avec blkid ?

Repackaging de l'initrd :
```
seb@debian:~/temp/TC$ cd isov1/tinycorelinux/tmpcore/
seb@debian:~/temp/TC/isov1/tinycorelinux/tmpcore$ sudo find | sudo cpio -o -H newc | gzip -2 > ../core.gz 
[sudo] Mot de passe de seb : 
34183 blocs


seb@debian:~/temp/TC$ sudo cp erasedisk.sh packaging/tmpcore/etc/skel/erasedisk.sh 
seb@debian:~/temp/TC$ sudo cp packaging/.profile packaging/tmpcore/etc/skel/.profile 

seb@debian:~/temp/TC$ cd packaging/tmpcore/
seb@debian:~/temp/TC/packaging/tmpcore$ sudo find | sudo cpio -o -H newc | gzip -2 > ../../isov1/tinycorelinux/core.gz 
34183 blocs

seb@debian:~/temp/TC/packaging/tmpcore$ cd ../..
seb@debian:~/temp/TC$ ./buildiso.sh && ./uploadiso.sh 
xorriso 1.5.2 : RockRidge filesystem manipulator, libburnia project.

Drive current: -outdev 'stdio:tmp.iso'
Media current: stdio file, overwriteable
Media status : is blank
Media summary: 0 sessions, 0 data blocks, 0 data, 20.3g free
Added to ISO image: directory '/'='/home/seb/temp/TC/isov1'
xorriso : UPDATE :      22 files added in 1 seconds
xorriso : UPDATE :      22 files added in 1 seconds
xorriso : NOTE : Copying to System Area: 432 bytes from file '/usr/lib/ISOLINUX/isohdpfx.bin'
libisofs: NOTE : Aligned image size to cylinder size by 224 blocks
ISO image produced: 13824 sectors
Written to medium : 13824 sectors at LBA 0
Writing to 'stdio:tmp.iso' completed successfully.

tmp.iso                                                               100%   27MB  31.9MB/s   00:00  
```
my_content: le dossier pour ajouter les extensions tce. il se package ensuite en extensions.gz puis est chargé par grup / isolinux (il faut modifier leur config) par l'option initrd=/tinycorelinux/core.gz,/tinycorelinux/extensions.gz

extensions.gz se construit avec buildextensions.sh

### Passage en mode root
option de chargement du kernel: superuser, permet de rester connecter en root. Pour le script de nettoyage du disque, c'est mieux


### Fichier d'effacement du disque
```
#!/bin/sh

tce-load -i /opt/tce/optional/util-linux.tcz
tce-load -i /opt/tce/optional/pv.tcz

# DRIVE=`dmesg | grep -i "attached.*disk" | sed 's/^.*\[\(.*\)\].*$/\1/'`
DRIVE=`lsblk | grep disk | grep -v SWAP | cut -d' ' -f0`
SIZE=`lsblk -b | grep disk | grep -v SWAP | awk '{print $4}'

echo "erasing all data on $DRIVE. Sure?"
echo "time dd if=/dev/random | pv -s $SIZE | dd of=/dev/$DRIVE"

read -n 1 -p "Continue? (y|N)" "continue"
echo $continue
```
Il n'est pas encore au point car il doit se lancer automatiquement
Il nécessite le chargement de 2 extensions : util-linux.tcz pour lsblk, et pv.tcz pour pv. pv sert à afficher la progression de la commande
