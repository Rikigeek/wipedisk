#!/bin/bash
# Created 2023-02-14 by setigeek
# update the TinyCoreLinux initrd (core.gz)

SRC=packaging/tmpcore/
TPL=template
IN=../../sources/core.gz
OUT=../../isov1/tinycorelinux/core.gz

if [ ! -d $SRC ]; then
  echo "$SRC doesn't exist"
  mkdir -p ${SRC}
fi

# Extract the original core.gz
pushd .
sudo chmod -R 777 ${SRC}
rm -Rf ${SRC}
mkdir -p ${SRC}
cd ${SRC}
zcat ${IN} | sudo cpio -i -H newc -d
popd

# root user skeleton
echo "Adding root folder items"
sudo cp $TPL/root/* $SRC/root
sudo chown -R root:root $SRC/root
sudo cp $TPL/root/.profile $SRC/root

# adding non standard application (lsblk & pv)
echo "Adding non standard applications (lsblk & pv)"
sudo cp $TPL/usr/local/bin/* $SRC/usr/local/bin
sudo cp $TPL/usr/local/lib/* $SRC/usr/local/lib
sudo chmod +x $SRC/usr/local/bin/lsblk
sudo chmod +x $SRC/usr/local/bin/pv

echo "Preparing the build: Moving to $SRC"
cd $SRC
echo "Building core initrd into $OUT"
sudo find . | sudo cpio -o -H newc | gzip -2 > $OUT
