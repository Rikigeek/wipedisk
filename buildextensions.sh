#!/bin/sh

MBR=/usr/lib/ISOLINUX/isohdpfx.bin
SRC=sources/extensions/
TMP=packaging/extensions/
OUT=../../isov1/tinycorelinux/extensions.gz

if [ ! -d ${TMP} ]; then
    echo "${TMP} doesn't exist"
    mkdir -p ${TMP}
fi

ls -1 $SRC/opt/tce/optional | sudo tee $SRC/opt/tce/onboot.lst
cp -R ${SRC} ${TMP}
sudo chown -R root:root ${TMP}
pushd .
cd ${TMP}
sudo find . | sudo cpio -o -H newc | gzip -2 > $OUT
sudo chmod -R 777 .
popd
rm -Rf ${TMP}