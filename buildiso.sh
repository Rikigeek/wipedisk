#!/bin/sh
# Created 2023-01-16 by setigeek
# Build an iso image for a customized tiny core linux distribution
# MBR boot is ok
# 
# TODO: efi boot broken

MBR=/usr/lib/ISOLINUX/isohdpfx.bin
SRC=isov1/
KERNEL=kernel/linux-5.15.10/arch/x86_64/boot/bzImage
cp ${KERNEL} $SRC/tinycorelinux/vmlinuz
# all the path are based inside the iso, except for the isohybrid-mbr option
xorriso -as mkisofs \
  -isohybrid-mbr ${MBR} \
  -c isolinux/boot.cat \
  -b isolinux/isolinux.bin \
  -no-emul-boot \
  -boot-load-size 4 \
  -boot-info-table \
  -eltorito-alt-boot \
  -e boot/grub/efi.img \
  -no-emul-boot \
  -isohybrid-gpt-basdat \
  -o tmp.iso \
  ${SRC}
