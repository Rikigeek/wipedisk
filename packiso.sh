#!/bin/sh 

mkisofs -l -J -R -V TC-custom \
 -no-emul-boot -boot-load-size 4 -boot-info-table \
 -eltorito-alt-boot \
 -eltorito-platform 0xEF -eltorito-boot boot/isolinux/efiboot.img \
 -b boot/isolinux/isolinux.bin \
 -c boot/isolinux/boot.cat \
 -o TC-remastered.iso \
 newiso
