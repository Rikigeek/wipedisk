#!/bin/sh

echo "Waiting for full initialization"
sleep 5
TYPE=disk

DRIVES=$(lsblk | grep $TYPE | grep -v SWAP | awk '{print $1}')
SIZESH=$(lsblk | grep $TYPE | grep -v SWAP | awk '{print $4}')
REMOVS=$(lsblk | grep $TYPE | grep -v SWAP | awk '{print $3}')

i=1
for f in ${DRIVES}; do
  sizeh=$(echo $SIZESH | cut -d' ' -f${i})
  echo "($i) $f [${sizeh}]"
  i=$((i+1))
done
read -n 1 -p "> Select the disk to swipe: " "selected"

DRIVE=$(echo $DRIVES | cut -d' ' -f$selected)
SIZEH=$(echo $SIZESH | cut -d' ' -f$selected)
echo " Selected: $selected $DRIVE $SIZEH" 

if [ "$DRIVE" = "" ]; then
  echo "    No selection or incorrect selection"
  exit
fi

SIZE=`lsblk -b | grep $TYPE | grep $DRIVE | grep -v SWAP | awk '{print $4}'`

echo "!!!!! Erasing all data on $DRIVE. Size is $SIZEH !!!!!"
echo "time dd if=/dev/random | pv -s $SIZE | dd of=/dev/$DRIVE bs=4M"

read -n 1 -p "> There is no way back. Are you sure? (y|N) " "continue"
echo 
echo "Alea jacta est"
if [ "$continue" = "y" ]; then
  echo "Swipping disk $DRIVE. Please wait"
  time dd if=/dev/random | pv -s $SIZE | dd of=/dev/$DRIVE bs=4M
else
  echo "Skipping..."
fi
echo "You can now reboot computer on a installation disk (please don't install Windows)"
echo " or you can type 'sh erasedisk.sh' to run a wipe disk again"
