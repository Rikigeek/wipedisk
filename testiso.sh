#!/bin/bash
# Start Qemu to test the boot
# 2 modes. The default one (no UEFI), and UEFI
# ./testiso.sh [UEFI]
if [ -z $1 ]; then
    iso=tmp.iso
else
    iso=$1
fi
echo "Starting $iso"

qemu-img create -f raw sata.disk 20M
qemu-img create -f raw scsi.disk 30M

# Options are
# Display (for the UI): we zoom the vm box with the window size
# 1024 M for RAM
# a virtio-scsi-pci device 
# a scsi-hd device, attached to the virtio-scsi-pci 
# attach the disk scsi.disk to the scsi-hd device
# a sata.disk file attached as a disk
# specification for the boot order. We chose cdrom (d) as first choice
# a cdrom, to boot on
qemu-system-x86_64 \
     -display gtk,zoom-to-fit=on \
     -m 1024 \
     -device virtio-scsi-pci,id=scsi \
     -device scsi-hd,drive=hd \
     -drive if=none,id=hd,file=scsi.disk,format=raw \
     sata.disk \
     -boot order=d \
     -cdrom $iso
